/*
 * Vostok System Updater
 * Copyright (C) 2017-2018  The Vostok Project
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.airpost.relan.systemupdater;

import android.app.DownloadManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import android.util.Pair;
import java.io.IOException;
import java.util.Scanner;


public class DownloadCompleteReceiver extends BroadcastReceiver {

    private static final String TAG = "SystemUpdater.DCR";
    private static final int NOTIFICATION_ID = 1337;

    @Override
    public void onReceive(Context context, Intent intent) {
        long id = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, -1);
        DownloadManager.Query query = new DownloadManager.Query()
                .setFilterById(id)
                .setFilterByStatus(DownloadManager.STATUS_SUCCESSFUL);
        DownloadManager downloadManager = (DownloadManager) context
                .getSystemService(Context.DOWNLOAD_SERVICE);

        try (Cursor cursor = downloadManager.query(query)) {
            if (cursor != null && cursor.moveToFirst()) {
                Uri uri = getLocalUri(cursor);
                switch (getMediaType(cursor)) {
                case "text/plain":
                    Log.d(TAG, "Index downloaded to " + uri);
                    handleIndex(context, uri);
                    return;
                case "application/zip":
                    Log.d(TAG, "Update downloaded to " + uri);
                    handleUpdate(context, uri);
                    return;
                default:
                    Log.e(TAG, "Unexpected MIME type " + getMediaType(cursor));
                    return;
                }
            }
        }
        Log.e(TAG, "Downloading failed");
    }

    private String getMediaType(Cursor cursor) {
        int column = cursor.getColumnIndex(DownloadManager.COLUMN_MEDIA_TYPE);
        return cursor.getString(column);
    }

    private Uri getLocalUri(Cursor cursor) {
        int column = cursor.getColumnIndex(DownloadManager.COLUMN_LOCAL_URI);
        return Uri.parse(cursor.getString(column));
    }

    private void handleIndex(Context context, Uri uri) {
        ContentResolver resolver = context.getContentResolver();
        String thisBuild = Build.ID + "-" + Build.VERSION.INCREMENTAL;

        try (Scanner scanner = new Scanner(resolver.openInputStream(uri))) {
            for (;;) {
                Pair<String, String> builds = getNextPair(scanner);
                if (builds.first == null || builds.second == null) {
                    break;
                }
                if (builds.first.equals(thisBuild)) {
                    Log.d(TAG, "Found an update for " + thisBuild);
                    downloadUpdate(context, builds.second);
                    return;
                }
            }
            Log.d(TAG, "No updates found for " + thisBuild);
        } catch (IOException e) {
            Log.e(TAG, "Failed to read " + uri);
        } finally {
            resolver.delete(uri, null, null);
        }
    }

    private Pair getNextPair(Scanner scanner) {
        return new Pair(scanner.hasNext() ? scanner.next() : null,
                        scanner.hasNext() ? scanner.next() : null);
    }

    private void handleUpdate(Context context, Uri uri) {
        Intent notifyIntent = new Intent(context, InstallUpdateActivity.class)
                .setData(uri);
        String title = context.getString(R.string.notif_ready_title);
        String content = context.getString(R.string.notif_ready_text);

        Notification.Builder builder = new Notification.Builder(context)
                .setContentIntent(PendingIntent.getActivity(context, 0,
                        notifyIntent, PendingIntent.FLAG_UPDATE_CURRENT))
                .setSmallIcon(R.drawable.ic_system_update)
                .setContentTitle(String.format(title, getUpdateBuild(uri)))
                .setStyle(new Notification.BigTextStyle().bigText(content))
                .setCategory(Notification.CATEGORY_SYSTEM)
                .setLocalOnly(true)
                .setOngoing(true)
                .setVisibility(Notification.VISIBILITY_PUBLIC);

        Log.d(TAG, "Posting a notification to install");
        Utils.getNotificationManager(context).notify(NOTIFICATION_ID,
                builder.build());
    }

    private void downloadUpdate(Context context, String build) {
        Uri uri = getUpdateUri(context, build);
        String title = context.getString(R.string.notif_downloading_title);
        DownloadManager.Request request = Utils.newDownloadRequest(uri)
                .setDestinationUri(getUpdateDestination(context, build))
                .setTitle(String.format(title, build));

        Log.d(TAG, "Downloading " + uri);
        Utils.getDownloadManager(context).enqueue(request);
    }

    private Uri getUpdateUri(Context context, String build) {
        return Uri.parse(String.format("%s/%s/%s.zip",
                                       context.getString(R.string.server_url),
                                       Build.DEVICE, build));
    }

    private Uri getUpdateDestination(Context context, String build) {
        return Uri.parse(String.format("file://%s/%s.zip",
                                       context.getExternalCacheDir(), build));
    }

    private String getUpdateBuild(Uri uri) {
        return uri.getLastPathSegment().replace(".zip", "");
    }

}
