/*
 * Vostok System Updater
 * Copyright (C) 2017-2018  The Vostok Project
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.airpost.relan.systemupdater;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.util.Log;


public class StartPeriodicJob {

    private static final String TAG = "SystemUpdater.SPJ";
    private static final long UPDATES_CHECK_INTERVAL = 24; // In hours
    private static final int JOB_ID = 2640;

    public static void start(Context context) {
        JobScheduler scheduler = (JobScheduler)
                context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
        ComponentName component = new ComponentName(context.getPackageName(),
                PeriodicJobService.class.getName());
        JobInfo.Builder job = new JobInfo.Builder(JOB_ID, component)
                .setPeriodic(UPDATES_CHECK_INTERVAL * 60 * 60 * 1000)
                .setRequiredNetworkType(JobInfo.NETWORK_TYPE_UNMETERED);
        if (scheduler.schedule(job.build()) == JobScheduler.RESULT_SUCCESS) {
            Log.d(TAG, "Scheduled check for updates job");
        } else {
            Log.e(TAG, "Failed to schedule check for updates job");
        }
    }

}
