/*
 * Vostok System Updater
 * Copyright (C) 2017-2018  The Vostok Project
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.airpost.relan.systemupdater;

import android.app.DownloadManager;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;


public class CheckForUpdates {

    private static final String TAG = "SystemUpdater.CFU";

    private static void cancelAllNotifications(Context context) {
        Utils.getNotificationManager(context).cancelAll();
    }

    private static void cancelAllDownloads(Context context) {
        DownloadManager.Query query = new DownloadManager.Query();
        DownloadManager downloadManager = Utils.getDownloadManager(context);

        try (Cursor cursor = downloadManager.query(query)) {
            if (cursor != null) {
                while (cursor.moveToNext()) {
                    downloadManager.remove(cursor.getLong(cursor
                            .getColumnIndex(DownloadManager.COLUMN_ID)));
                }
            }
        }
    }

    public static void start(Context context) {
        cancelAllNotifications(context);
        cancelAllDownloads(context);

        Uri uri = getIndexUri(context);
        DownloadManager.Request request = Utils.newDownloadRequest(uri)
                .setTitle(context.getString(R.string.notif_check_title));

        Log.d(TAG, "Downloading " + uri);
        Utils.getDownloadManager(context).enqueue(request);
    }

    private static Uri getIndexUri(Context context) {
        return Uri.parse(String.format("%s/%s/index.txt",
                                       context.getString(R.string.server_url),
                                       Build.DEVICE));
    }

}
