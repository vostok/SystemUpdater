/*
 * Vostok System Updater
 * Copyright (C) 2017  The Vostok Project
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.airpost.relan.systemupdater;

import android.app.AlertDialog;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.BatteryManager;
import android.os.Bundle;
import android.os.RecoverySystem;
import android.util.Log;
import java.io.File;
import java.io.IOException;


public class InstallUpdateActivity extends Activity {

    private static final String TAG = "SystemUpdater.IUA";
    private static final int BATTERY_LEVEL_MIN = 30;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        int batteryLevel = getBatteryLevel();
        if (batteryLevel >= BATTERY_LEVEL_MIN) {
            Log.d(TAG, "Battery level " + batteryLevel + "% is OK");
            installUpdate(getIntent().getData());
        } else {
            Log.e(TAG, "Battery level " + batteryLevel + "% is too low");
            showAlertDialog(String.format(getString(R.string.error_battery),
                                          BATTERY_LEVEL_MIN));
        }
    }

    private int getBatteryLevel() {
        IntentFilter filter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent intent = registerReceiver(null, filter);
        return intent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);
    }

    private void installUpdate(Uri uri) {
        try {
            Log.d(TAG, "Calling recovery system");
            RecoverySystem.installPackage(this, getFileFromUri(uri));
            finish();
        } catch (IOException e) {
            Log.e(TAG, "Failed to install: " + e.getMessage());
            showAlertDialog(getString(R.string.error_install));
        }
    }

    private File getFileFromUri(Uri uri) {
        // Recovery does not use any "storage emulation" layers
        return new File(uri.getPath().replace("/storage/emulated/",
                                              "/data/media/"));
    }

    private void showAlertDialog(String message) {
        AlertDialog alert = new AlertDialog.Builder(this)
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton(android.R.string.ok,
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            finish();
                            dialog.dismiss();
                        }
                    })
                .create();
        alert.show();
    }

}
