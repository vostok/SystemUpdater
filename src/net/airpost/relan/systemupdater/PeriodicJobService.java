/*
 * Vostok System Updater
 * Copyright (C) 2017-2018  The Vostok Project
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package net.airpost.relan.systemupdater;

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.provider.Settings;
import android.util.Log;


public class PeriodicJobService extends JobService {

    private static final String TAG = "SystemUpdater.PJS";
    private static final String OTA_DISABLE_AUTOMATIC_UPDATE_KEY =
            "ota_disable_automatic_update";

    @Override
    public boolean onStartJob(JobParameters params) {
        if (Settings.Global.getInt(getContentResolver(),
                                   OTA_DISABLE_AUTOMATIC_UPDATE_KEY, 0) == 0) {
            CheckForUpdates.start(this);
        } else {
            Log.d(TAG, "Updates are disabled in Developer options");
        }

        return false;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        return false;
    }

}
